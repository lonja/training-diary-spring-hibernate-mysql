package com.altronica.spring_boot_hibernate_mysql

import org.springframework.boot.CommandLineRunner
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.annotation.Bean


@SpringBootApplication
open class TestApplication {

    companion object {

        @JvmStatic fun main(args: Array<String>) {
            SpringApplication.run(TestApplication::class.java, *args)
        }
    }

    @Bean
    fun init(): CommandLineRunner {
        return CommandLineRunner {

        }
    }
}