package com.altronica.spring_boot_hibernate_mysql.domain.entity

import javax.persistence.*

@Table(name = "exercise", catalog = "test")
@Entity
data class Exercise(

        @Id
        @GeneratedValue
        val id: Long,

        @Column(unique = true, nullable = false)
        var title: String,

        @Column(unique = true, nullable = false)
        var description: String
)