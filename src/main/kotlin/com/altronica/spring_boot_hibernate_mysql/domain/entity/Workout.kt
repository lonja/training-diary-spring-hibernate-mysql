package com.altronica.spring_boot_hibernate_mysql.domain.entity

import java.util.*
import javax.persistence.*

@Table(name = "workout", catalog = "test")
@Entity
data class Workout(

        @Id
        @GeneratedValue
        val id: Long,

        @OneToMany
        var exercises: List<Exercise> = ArrayList<Exercise>()
)