package com.altronica.spring_boot_hibernate_mysql.domain.repository

import com.altronica.spring_boot_hibernate_mysql.domain.entity.Workout
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource

@RepositoryRestResource(path = "/workouts")
interface WorkoutRepository : JpaRepository<Workout, Long>